# coding: utf-8

#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import urllib2
import random


peersprovider = "http://step-test-krispop.appspot.com/peers"

engdict = {	"noun":["Australia", "Cork", "Dice", "Map", "Table", "Samurai"], 
			"verb":["do", "dream", "catch" ,"run", "work", "leak", "sleep"],
			"adjective":["asian", "beautiful", "deep" ,"fine", "impossible", "blue", "young"],
			"name":["Alice", "Yuki", "I"]
			}

madliblist = [	"<name> was beginning to <verb> very tired of sitting by her sister on the bank.",
				"When you <verb> the <adjective>, whatever remains, however improbable, must be the <noun>.",
				"There are three kinds of lies: <noun>, damned lies, and <noun>."
			]


class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write("convert?message=...")


class ConvertHandler(webapp2.RequestHandler):
    def get(self):
		message = self.request.get("message")
		li = list(message)
		li.sort()
		message = "".join(li)
		self.response.write(message)


class ShowHandler(webapp2.RequestHandler):
    def get(self):
		response = urllib2.urlopen(peersprovider)
		converted = response.read()
		urls = converted.split("\n")

		message = self.request.get("message")
		for x in urls:
			try:
				url = x + "/convert?message=" + message
				response = urllib2.urlopen(url)
				converted = response.read()
				writeurl = x + "<br>"
				self.response.write(writeurl)
				converted = converted  + "<br>"
				self.response.write(converted)

			except urllib2.HTTPError, e:
				pass
			except urllib2.URLError, e: # サーバに接続できない場合に発生
				pass




class GetwordHandler(webapp2.RequestHandler):
	def get(self):
		pos = self.request.get("pos")

		if engdict.has_key(pos) == True:
			poslist = engdict[pos]
			i = random.randint(0,len(poslist)-1)
			self.response.write(poslist[i])
		else:
			keylist = engdict.keys()
			i = random.randint(0,len(keylist)-1)
			poslist = engdict[keylist[i]]
			j = random.randint(0,len(poslist)-1)
			self.response.write(poslist[j])



class MadlibHandler(webapp2.RequestHandler):
	def get(self):
		engpos = engdict.keys()
		s = random.randint(0,len(madliblist)-1)
		sentence = madliblist[s]

		response = urllib2.urlopen(peersprovider)
		converted = response.read()
		urls = converted.split("\n")


		for i in range(0, len(engpos)):
			searchword = "<" + engpos[i] + ">"
			n = sentence.count(searchword)

			for j in range(0, n):			
				try:
					l = random.randint(0,len(urls)-1)
					url = urls[l] + "/getword?pos=" + engpos[i]
					response = urllib2.urlopen(url)
					converted = response.read()
					sentence = sentence.replace(searchword, converted,1)
				except urllib2.HTTPError, e:
					j = j-1
				except urllib2.URLError, e: # サーバに接続できない場合に発生
					j = j-1

		self.response.write(sentence)


app = webapp2.WSGIApplication([
	("/", MainHandler),
	("/convert", ConvertHandler),
	("/show", ShowHandler),
	("/getword", GetwordHandler),
	("/madlib", MadlibHandler),
], debug=True)